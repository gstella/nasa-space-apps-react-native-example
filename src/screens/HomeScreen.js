import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { StyledText } from '../components/StyledText';
import {
  Container,
  Content,
  H1,
  Form,
  Item,
  Label,
  Input,
  Button,
} from 'native-base';
import Tarea from '../components/Tarea';
import { FlatList } from 'react-native-gesture-handler';

const style = StyleSheet.create({
  boton: {
    marginVertical: 10
  },
})

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nombreTarea: '',
      tareas: [],
    }
  }

  onNombreTareaChange = (value) => {
    this.setState({
      nombreTarea: value,
    })
  }

  onAgregarPress = () => {
    const newTareas = this.state.tareas.concat({
      id: `${Math.random()}`,
      nombre: this.state.nombreTarea,
    });
    this.setState({
      tareas: newTareas,
      nombreTarea: '',
    });
    console.log(this.state.tareas)
  }

  onDelete = (tarea) => {
    const newTareasList = this.state.tareas.filter(t => t.id !== tarea.id);
    this.setState({
      tareas: newTareasList,
    })
  }

  render() {
    return (
      <Container>
        <Content>
          <H1>TODO List</H1>
          <Form>
            <Item floatingLabel>
              <Label>Nombre tarea</Label>
              <Input
                value={this.state.nombreTarea}
                onChangeText={this.onNombreTareaChange}
              />
            </Item>
          </Form>
          <Button
            block
            style={style.boton}
            onPress={this.onAgregarPress}
          >
            <Text>Agregar tarea</Text>
          </Button>
          <FlatList
            data={this.state.tareas}
            renderItem={({ item }) => <Tarea tarea={item} onDelete={this.onDelete} />}
            keyExtractor={item => item.id}
          />
        </Content>
      </Container>
    );
  }
}

