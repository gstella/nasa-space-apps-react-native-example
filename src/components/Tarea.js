import React from 'react';
import { ListItem, Left, Icon, Body, Right, Text, Button } from 'native-base';

export default class Tarea extends React.Component {
  render() {
    return (
      <ListItem icon>
        <Left>
          <Icon active name="md-checkmark" />
        </Left>
        <Body>
          <Text>{this.props.tarea.nombre}</Text>
        </Body>
        <Right>
          <Button onPress={() => this.props.onDelete(this.props.tarea)}>
            <Icon name="md-trash" />
          </Button>
        </Right>
      </ListItem>
    );
  };
}